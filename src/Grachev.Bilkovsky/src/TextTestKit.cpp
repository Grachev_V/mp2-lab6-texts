// TextTestKit.cpp - ������ ������������� ������ TText

#include "include/TText.h"

int main(int argc, char* argv[]) {
    setlocale(LC_ALL, "Russian");
    string fileName;

    if (argc < 2) {
        cout << "������� ��� ����� � ������� >> ";
        cin >> fileName;
        cout << endl;
    }
    else
        fileName = argv[1];

    PTText pText;
    TTextLink::InitMemSystem(50);
    TText text;

    text.Read(fileName);
    cout << "�����, ����������� �� ����� " << fileName << ":\n";
    cout << endl;
    text.Print();
    cout << endl;

    cout << "���������������� ����� :\n";
    cout << endl;
    pText = text.GetCopy();
    pText->GoFirstLink();
    pText->SetLine("����. 2 ����");
    pText->InsDownLine("������ 1");
    pText->GoDownLink();
    pText->InsDownLine("1.1 ��������");
    pText->GoDownLink();
    pText->InsNextLine("1.2 ������� ������");
    pText->GoNextLink();
    pText->InsDownLine("1. �����������");
    pText->GoDownLink();
    pText->InsNextLine("2. ���������");
    pText->GoFirstLink();
    pText->GoDownLink();
    pText->GoNextLink();
    pText->GoDownLink();
    pText->GoDownLink();
    pText->InsNextLine("2. ����������");
    pText->GoNextLink();
    pText->DelNextLine();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->DelNextSection();
    pText->Print();
    
    fileName = "output.txt";
    pText->Write(fileName);
    cout << "\n���������������� ����� �������� � ���� " << fileName << endl;
    return 0;
}
