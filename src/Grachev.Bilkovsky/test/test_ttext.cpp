﻿#include "gtest/gtest.h"

#include <iostream>
#include <include/TText.h>
#include <include/TTextLink.h>
#include <src/TText.cpp>
#include <src/TTextLink.cpp>

TEST(TText, Can_Create_Empty_Text) {
    ASSERT_NO_THROW(TText _text);
}

TEST(TText, Can_Create_Not_EmptyText) {
    const string str = "qqq";

    TTextLink::InitMemSystem(1);
    TTextLink Link = TTextLink(str);

    ASSERT_NO_THROW(TText _text(&Link));
}

TEST(TText, RetCode_Of_New_Text_Is_OK) {
    const string str = "qqq";

    TTextLink::InitMemSystem(1);
    TTextLink Link = TTextLink(str);
    TText _text(&Link);

    ASSERT_TRUE(_text.GetRetCode() == TextOK);
}

TEST(TText, Can_Set_Current_Line) {
    const string str1 = "qqq", str2 = "abc";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str1);
    TText _text(&Link);
    _text.GoFirstLink();

    ASSERT_NO_THROW(_text.SetLine(str2));
}

TEST(TText, Can_Get_Current_Line)
{
    const string str1 = "qqq", str2 = "abc";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str1);
    TText _text(&Link);
    _text.GoFirstLink();

    _text.SetLine(str2);
    bool equality_of_strings = (_text.GetLine() == str2) 
                            && (_text.GetLine() != str1);

    ASSERT_TRUE(equality_of_strings);
}

TEST(TText, Can_Go_Down_Link) {
    const string str1 = "Section 1", str2 = "Section 1.1";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str1);
    TText _text(&Link);
    _text.GoFirstLink();

    _text.InsDownLine(str2);
    _text.GoDownLink();

    EXPECT_EQ(str2, _text.GetLine());
}

TEST(TText, Can_Go_Next_Link) {
    const string str1 = "Section 1", str2 = "Section 2";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str1);
    TText _text(&Link);
    _text.GoFirstLink();

    _text.InsNextLine(str2);
    _text.GoNextLink();

    EXPECT_EQ(str2, _text.GetLine());
}

TEST(TText, Can_Go_First_Link) {
    string str = "Section 1";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str);
    TText _text(&Link);
    _text.GoFirstLink();

    for (int i = 0; i < 3; i++) {
        str += "1";
        _text.InsNextLine(str);
        _text.GoNextLink();
    }

    bool isFirstLink = (_text.GetLine() == "Section 1111");
    _text.GoFirstLink();
    isFirstLink = isFirstLink && (_text.GetLine() == "Section 1");
    ASSERT_TRUE(isFirstLink);
}

TEST(TText, Can_Go_Previous_Link) {
    string str = "Section 1";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str);
    TText _text(&Link);
    _text.GoFirstLink();

    for (int i = 0; i < 3; i++) {
        str += "1";
        _text.InsNextLine(str);
        _text.GoNextLink();
    }

    _text.GoPrevLink();
    EXPECT_EQ("Section 111", _text.GetLine());
}

TEST(TText, Set_Error_Code_When_Go_Nonexistent_Link) {
    const string str1 = "Section 1";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str1);
    TText _text(&Link); _text.GoFirstLink();
    bool error_codes;

    _text.GoDownLink();
    error_codes = _text.GetRetCode() == TextNoDown;
    _text.GoNextLink();
    error_codes = error_codes && (_text.GetRetCode() == TextNoNext);

    ASSERT_TRUE(error_codes);
}


TEST(TText, Can_Delete_Down_Line)
{
    // Arrange
    const string str1 = "Hello world!";
    const string str2 = "Hello!";
    const string str3 = "World welcomes you";
    TTextLink::InitMemSystem(4);
    TText T;
    T.GoFirstLink();
    T.InsDownLine(str1);
    T.GoDownLink();
    T.InsDownLine(str2);
    T.InsDownLine(str3);

    T.GoDownLink();
    T.DelDownLine();

    T.GoFirstLink();
    T.GoDownLink();
    EXPECT_EQ(T.GetLine(), str1);
    T.GoDownLink();
    EXPECT_EQ(T.GetLine(), str3);
}

TEST(TText, Can_Delete_Down_Section)
{
    const string sec1 = "section 1";
    const string sec2 = "section 1.1";
    const string sec3 = "section 1.2";
    const string str1 = "string 1";
    const string str2 = "string 2";
    TTextLink::InitMemSystem(6);
    TText T;
    T.GoFirstLink();
    T.InsDownLine(sec1);
    T.GoDownLink();
    T.InsDownLine(sec3);
    T.InsDownLine(sec2);
    T.GoDownLink();
    T.InsDownLine(str2);
    T.InsDownLine(str1);

    T.DelDownSection();

    T.GoFirstLink();
    T.GoDownLink();
    T.GoDownLink();
    EXPECT_EQ(T.GoDownLink(), TextNoDown);
}

TEST(TText, Can_Delete_Next_Line)
{
    const string sec1 = "section 1";
    const string str1 = "string 1";
    const string str2 = "string 3";
    const string str3 = "string 2";
    TTextLink::InitMemSystem(5);
    TText T;
    T.GoFirstLink();
    T.InsDownLine(sec1);
    T.GoDownLink();
    T.InsDownSection(str1);
    T.GoDownLink();
    T.InsNextLine(str2);
    T.InsNextLine(str3);

    T.DelNextLine();

    T.GoFirstLink();
    T.GoDownLink();
    T.GoDownLink();
    bool equality_of_strings = T.GetLine() == str1;
    T.GoNextLink();
    equality_of_strings = equality_of_strings && (T.GetLine() == str2);

    ASSERT_TRUE(equality_of_strings);
}

TEST(TText, Can_Delete_Next_Section)
{
    const string sec1 = "section 1";
    const string sec2 = "section 2";
    const string str1 = "string 1";
    const string str2 = "string 2";
    const string str3 = "string 3";
    TTextLink::InitMemSystem(7);
    TText T;

    T.GoFirstLink();
    T.InsDownLine(sec1);
    T.GoDownLink();
    T.InsNextLine(str2);
    T.InsNextLine(str1);
    T.InsNextSection(sec2);
    T.InsNextLine(str3);

    T.GoNextLink();
    T.DelNextSection();

    T.GoFirstLink();
    T.GoDownLink();
    T.GoNextLink();
    EXPECT_EQ(T.GetLine(), str3);
}

TEST(TText, Can_Use_Iterator) {
    string str = "Section 1";
    TTextLink::InitMemSystem(10);
    TTextLink Link = TTextLink(str);
    TText _text(&Link); _text.GoFirstLink();
    for (int i = 0; i < 3; i++) {
        _text.InsNextLine(str);
        _text.GoNextLink();
        str += "1";
    }

    bool correctness = true;
    str = "Section 1";
    for (_text.Reset(); _text.IsTextEnded(); _text.GoNext()) {
        correctness = correctness && (str == _text.GetLine());
        correctness = correctness && !_text.IsTextEnded();
        str += "1";
    }

    ASSERT_TRUE(correctness);
}
